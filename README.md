# Sweatworks - Test Fullstack - API

## Clone the repository using HTPPS protocol

git clone https://lexp2001@bitbucket.org/lexp2001/sweatworks_api.git

## Install dependencies

cd sweatworks_api/

npm install

## Development server

Run `npm start` for a dev server. Your port 3000 must to be free for this test

npm start

## Create datatables structure and initial records

### Check the server

http://localhost:3000/

### Create `Author's datatables` and fill with initial data

http://localhost:3000/authors/createAuthorTable

http://localhost:3000/authors/reset

### Check the data inserted

http://localhost:3000/authors/showAuthors

### Create `Author's datatables` and fill with initial data

http://localhost:3000/publications/createPublicationTable

http://localhost:3000/publications/reset

### Check the data inserted

http://localhost:3000/publications/showPublications/1

## Testing

### Using supertest for testing

Stop the npm process (ctrl + c), the test using same port 3000, Enter in the project directory and type:

node_modules/.bin/mocha

There is also a script file that contains that instruction, just write: 

. test.sh

### Notes

This is a very simple API implementation, in a real scenario, it would be necessary to add some other functionalities such as error handling, validations, etc.