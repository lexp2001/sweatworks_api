// Author microservices

var sqlite3 = require('sqlite3').verbose(),
db = new sqlite3.Database('sweatworks'),
AUTHORS = {};

// Initialize author datatable
AUTHORS.createAuthorTable = function()
{
	db.run("DROP TABLE IF EXISTS authors");
	db.run("CREATE TABLE IF NOT EXISTS authors (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT, birthday TEXT)");
	console.log("La tabla usuarios ha sido correctamente creada");
}

// Insert an author
AUTHORS.insertAuthor = function(authorData)
{
	var stmt = db.prepare("INSERT INTO authors VALUES (?,?,?,?)");
	stmt.run(null,authorData.name,authorData.email, authorData.birthday);
    stmt.finalize();
}

// Update an author
AUTHORS.updateAuthor = function(authorData)
{
	var stmt = db.prepare("UPDATE authors SET name = ?, email = ?, birthday = ? WHERE id = ?");
	stmt.run(authorData.name,authorData.email, authorData.birthday, authorData.id);
    stmt.finalize();
}

// Get all authors
AUTHORS.getAuthors = function(callback)
{
	db.all("SELECT * FROM authors ORDER BY id DESC", function(err, rows) {
		if(err)
		{
			throw err;
		}
		else
		{
			callback(null, rows);
		}
	});
}

// Delete an author and it's publications
AUTHORS.deleteAuthor = function( id )
{
	var stmt = db.prepare("DELETE FROM publications WHERE id_author = ?");
	stmt.run(id);
	stmt.finalize();
	
	var stmt = db.prepare("DELETE FROM authors WHERE id = ?");
	stmt.run(id);
    stmt.finalize();
}

module.exports = AUTHORS;