// Publication microservices


var sqlite3 = require('sqlite3').verbose(),
db = new sqlite3.Database('sweatworks'),
PUBLICATIONS = {};

// Create a new table
PUBLICATIONS.createPublicationTable = function()
{
	db.run("DROP TABLE IF EXISTS publications");
	db.run("CREATE TABLE IF NOT EXISTS publications (id INTEGER PRIMARY KEY AUTOINCREMENT, id_author INTEGER, date TEXT, body TEXT, title TEXT)");
	console.log("La tabla publications ha sido correctamente creada");
}

// Insert a new publication
PUBLICATIONS.insertPublication = function(publicationData)
{
	var stmt = db.prepare("INSERT INTO publications VALUES (?,?,?,?,?)");
	stmt.run(null,publicationData.id_author,publicationData.date, publicationData.body, publicationData.title);
    stmt.finalize();
}

// Get Publications by author id
PUBLICATIONS.getPublications = function(id, callback)
{
	db.all("SELECT * FROM publications WHERE id_author =" + id, function(err, rows) {
		if(err)
		{
			throw err;
		}
		else
		{
			callback(null, rows);
		}
	});
}

// Get paginated publications by author id using filtering options
PUBLICATIONS.getPagPublications = function(body, callback)
{
	// Parameters for filtering and ordering
	var id = body.id;
	var limit =  body.limit;
	var offset = Number(body.pag) * Number(limit);
	var order = body.order;
	var search = body.search;

	db.all("SELECT COUNT(*) AS total FROM publications WHERE id_author =" + id +" AND title LIKE '%"+search+"%'", 

		function(err, row) {
			if(err)
			{
				throw err;
			}
			else
			{
				db.all("SELECT * FROM publications WHERE id_author =" + id + " AND title LIKE '%"+search+"%'" +
					" ORDER BY " + order + " ASC " +
					" LIMIT " + offset +", " + limit, 
					function(err, rows) {

					if(err)
					{
						throw err;
					}
					else
					{
						callback(null, {total:row[0].total, publications:rows});
					}
				});
				
			}
		});

}

// Update a publication
PUBLICATIONS.updatePublication = function(publicationData)
{
	var stmt = db.prepare("UPDATE publications SET title = ?, body = ?, date = ?, id_author = ?  WHERE id = ?");
	stmt.run(publicationData.title,publicationData.body, publicationData.date, publicationData.id_author, publicationData.id);
    stmt.finalize();
}

// Delete a publication by it's id
PUBLICATIONS.deletePublication = function(id )
{
	var stmt = db.prepare("DELETE FROM publications WHERE id = ?");
	stmt.run(id);
    stmt.finalize();
}

module.exports = PUBLICATIONS;