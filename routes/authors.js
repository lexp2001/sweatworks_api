// Author model

var express = require('express');
var router = express.Router();
var AUTHOR = require('../models/authors');

/* GET authors  */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

/* Create one author. */
router.post('/createAuthor', function(req, res, next) {
    AUTHOR.insertAuthor(req.body);
    res.send({'resp':'Ok'});
});

/* Update an author */
router.put('/updateAuthor', function(req, res, next) {
    AUTHOR.updateAuthor(req.body);
    res.send({'resp':'Ok'});
});

// Create author table 
router.get("/createAuthorTable", function(req, res){
    AUTHOR.createAuthorTable();
    res.send('Table Author created');
});
  
// Insert one author for data testing
router.get("/reset", function(req, res)
{
    //AUTHOR.createAuthorTable();

    AUTHOR.insertAuthor(
        {
            name:"Lex Phulop",
            email:"lexp2001@gmail.com",
            birthday:"08/10/1991"
        }
    );
    
    res.send('Author Table Reseted');
});

// Get authors
router.get("/showAuthors", function(req, res){
    AUTHOR.getAuthors(function(error, data)
    {
        res.send(data);
    });
});

  
// Delete author by id
router.delete("/deleteAuthor/:id", function(req, res){
    AUTHOR.deleteAuthor(req.params.id);
    res.send({'resp':'Ok'});
});    
  
  
module.exports = router;