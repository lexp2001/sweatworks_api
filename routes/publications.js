// Main publications route file for microservices consumming

var express = require('express');
var router = express.Router();
var PUBLICATION = require('../models/publications');

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

/* Create publication */
router.post('/createPublication', function(req, res, next) {
    PUBLICATION.insertPublication(req.body);
    res.send({'resp':'Ok'});
});

/* Create publication data table */
router.get("/createPublicationTable", function(req, res){
    PUBLICATION.createPublicationTable();
    res.send('Table Publication created');
});
  
/* Update a publication */
router.put('/updatePublication', function(req, res, next) {
    PUBLICATION.updatePublication(req.body);
    res.send({'resp':'Ok'});
});

// Insert some publications for testing pourpose 
router.get("/reset", function(req, res)
{
    //PUBLICATION.createPublicationTable();

    PUBLICATION.insertPublication(
        {
            id_author:"1",
            date:"2014-11-14T04:14",
            title:"Getting Started with Angular - Second Edition",
            body:"Key Features Up to date with the latest API changes introduced by Angular 2 and 4 Get familiar with the improvements to directives, change detection, dependency injection, router, and more Understand Angular's new component-based architecture Start using TypeScript to supercharge your Angular applications Book Description I'm delighted to see this new update and hope it helps you build amazing things with Angular. - Misko Hevery, Creator of AngularJS and Angular Angular is the modern framework you need to build performant and robust web applications. ",
        }
    );


    res.send('Publication Table Reseted');
});

// Get publications by author id
router.get("/showPublications/:id", function(req, res){
    PUBLICATION.getPublications(req.params.id, function(error, data)
    {
        res.send(data);
    });
});

// Get publications with pagination structure
router.post("/showPagPublications", function(req, res){
    PUBLICATION.getPagPublications(req.body,function(error, data)
    {
        res.send(data);
    });
});

// Delete especific publications
router.delete("/deletePublication/:id", function(req, res){
    PUBLICATION.deletePublication(req.params.id);
    res.send({'resp':'Ok'});
});  


module.exports = router;