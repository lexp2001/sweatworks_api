var request = require('supertest')
    ,app = require('../app')
    ,id = ""

    
describe("Testing Authors", function(){

    //Create a new Author
    it("Testing POST Authors", function(done) {
        request(app).post("/authors/createAuthor")
        .send(
            {
                name:"Name of Testing",
                email:"testingemail@test.com",
                birthday:"08/22/1969"
            }
        )
        .set('Accept', 'application/json')
        .expect({'resp':'Ok'})
        .expect(200, done)
    });

    // Get all the Authors, '/authors/showAuthors' returns an array ordered by date (same as ordered by Id)
    it("Testing Get Authors", function(done) {
        request(app).get("/authors/showAuthors")
        .expect(function(res) {
            id = res.body[0].id; // This is the id create in the POST test (above), if the POST test fails this test continue with the latest record
          })
        .expect(200, done)
    });

    // Update the most recent created author
    it("Testing UPDATE Authors", function(done) {
        request(app).put("/authors/updateAuthor" )
        .send(
            {
                id: id, 
                name:"Name of Testing2",
                email:"testingemail@test2.com"
            }
        )
        .set('Accept', 'application/json')
        .expect({'resp':'Ok'})
        .expect(200, done)
    });

    // Delete the most recent created author
    it("Testing DELETE record in Authors", function(done) {
        request(app).delete("/authors/deleteAuthor/"+id )
        .expect({'resp':'Ok'})
        .expect(200, done)
    });

});

describe("Testing Publications", function(){

    // Create a new Publication, the name beginning with '000' guarantees 
    // that it will be the first record shown in the following tests,
    // case this test fails the next ones to be continue with another record
    
    it("Testing POST Publications", function(done) {
        request(app).post("/publications/createPublication")
        .send(
            {
                id_author:1,
                date:"2014-11-14T04:14",
                title:"0000000",
                body:"Testing API",
            }
        )
        .set('Accept', 'application/json')
        .expect({'resp':'Ok'})
        .expect(200, done)
    });
    

    // Get publications
    it("Testing get Publications with POST method", function(done) {
        request(app).post("/publications/showPagPublications")
        .send(
            {
                "id":1,
                "limit": 5,
                "pag": 0,
                "order":"title",
                "search":""
            }
        )
        .set('Accept', 'application/json')
        .expect(function(res) {
            id = JSON.parse(res.text).publications[0].id; // This is the id create in the POST test (above), if the POST test fails this test continue with the next record
        })
        .expect(200, done)
    });


    // Update the most recent created publication
    it("Testing UPDATE publications", function(done) {
        request(app).put("/publications/updatePublication" )
        .send(
            {
                id:id,
                id_author:1,
                date:"2014-11-14T04:14",
                title:"0000000",
                body:"Testing API after test",
            }
        )
        .set('Accept', 'application/json')
        .expect({'resp':'Ok'})
        .expect(200, done)
    });

    // Delete the most recent created author
    it("Testing DELETE record in Publications", function(done) {
        request(app).delete("/publications/deletePublication/"+id )
        .expect({'resp':'Ok'})
        .expect(200, done)
    });

});